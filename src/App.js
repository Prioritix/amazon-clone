import React,{ useEffect} from 'react';
import './App.css';
import Header from './components/header/Header';
import Home from './pages/home/Home';
import {BrowserRouter, Switch,Route} from 'react-router-dom';
import Checkout from './pages/checkout/Checkout';
import Login from './pages/login/Login';
import { auth } from './firebase/firebase';
import { useStateValue } from './contextAPI files/StateProvider';
import Payment from './pages/payment/Payment';
import {loadStripe} from "@stripe/stripe-js";
import {Elements} from "@stripe/react-stripe-js";
import Orders from './pages/orders/Orders';

const promise =loadStripe("pk_test_51HpX4KCkZaVLC0Mo6RZkYOpF3rDwAs0Rh6ptrYF0jOe6RrksWwY6asEdmhhnteMpxz2OAy8bShvo1BczCzT17R4100eBFHi4v7");

function App() {
  const [{},dispatch]=useStateValue();

  useEffect(()=>{
    auth.onAuthStateChanged(authuser=>{
      if (authuser){
        // the user in logged in
        dispatch({
          type:'SET_USER',
          user:authuser
        })
      }
      else{
        // the user is logged out
        dispatch({
          type:'SET_USER',
          user:null
        })
      }
    })

  },[])
  return (
    <BrowserRouter>
    <div className="app">
      
        <Switch>
          <Route exact path='/login' >
            <Login/>
          </Route>
          <Route exact path='/orders'>
            <Header/>
            <Orders/>
          </Route>
          <Route exact path='/checkout'>
            <Header/>
            <Checkout/>
          </Route>
          <Route exact path='/payment' >
            <Header/>
            <Elements stripe={promise}>
              <Payment/>
            </Elements>
            
          </Route>
          <Route path='/' >
            <Header/>
            <Home/>
          </Route>
        </Switch>
      
    </div>
    </BrowserRouter>
  );
}

export default App;

// hosted on https://clone-7043b.web.app/