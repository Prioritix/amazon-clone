import React from 'react';
import Product from '../../components/product/Product';
import './Home.css'

function Home() {
    return (
        <div className='home'>
            <div className="header__container">
                <img
                    className='home__image'
                    src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
                    alt=""/>
                
                <div className="home__row">
                    <Product
                        key={1}
                        id = {1} 
                        title='The lean startup:How constant innovation creates rapidly Successfull Businesses'
                        image='https://images-na.ssl-images-amazon.com/images/I/81-QB7nDh4L.jpg'
                        price={29.9}
                        rating={5}
                        />
                    
                    <Product 
                        key={2}
                        id = {2}
                        title='Kenwood kmix stand mixer for baking, stylish kitchen mixer with k-beater,Dough Hook and Wish, 5 liter glass bowl'
                        image='https://www.zippytom.com/pub/media/catalog/product/cache/94bf093be1747e2eb18d2b072ecf6ca6/0/2/02_1_3.jpg'
                        price={239.0}
                        rating={4}
                        />
                    
                </div>
                <div className="home__row">
                    <Product 
                        key={3}
                        id = {3}
                        title="Samsung LC49RG90SSUXEN 49' Curver LED Gaming Monitor"
                        image='https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SL1000_.jpg'
                        price={199.99}
                        rating={3}
                        />
                    
                    <Product 
                        key={4}
                        id = {4}
                        title='Amazon Echo (3rd generation) | smart speakeer with Alexa, charcol fabric'
                        image='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUTExMWFRIXFhYXGBgVEhUaGBcXGBYXFhcVFhUYHSggGRolGxgXITEhJSkrLy4uGB8zODMsOigtLysBCgoKDg0NFQ8PGDclHSUuLS4tMDc3LTc3OC0rKy4rNys0Lzc3NyswKzQtOC4tMi0tLzc3Ky0rKystMSswNystK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAwUBAgQHBgj/xABIEAABAgMEBgUIBwcDBAMAAAABABECITEDQVFhBHGBkaHwBQYSscEHMkJic9Hh8SIkNHKCstITFCNDUlOSg5OiFRYzVBdjwv/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAIREBAAICAgAHAAAAAAAAAAAAAAERAgMToTFBQ1FSU3H/2gAMAwEAAhEDEQA/APcUREBERAREQEWCtLW1EIeIgDEkAbygkRU1v1p0KCukWf4T2vyuuO0686CP5kR1WcfuQfSovlf+/dEu/aH8IH5ogo4vKDoo9C1OqGD9aUW+uRfHnyhaPdZW26z/AFpD5QLA/wAm23QeEStJb7BF8fF5QLEfybbdB+pYHlD0f+1bbrP9aUW+xRfIDyh6LfZ2w/DB+pSf9/6Jf+0H4AfyxFRbfVovmYOvWhH+ZENdnH4Bddh1s0KOmkQD7xMP5gELXaKKw0iGMdqCIRQ4wkEbwpHQZREQEREBERAREQEREBERAREKCh63dPfull2oQIrWKUEJLCVYjkMF5B01aaTpUXatrW0iNZAGEaoYQw2L6rygaUY9LML/AEbOGGEayDFEeLKgsog02pJrmm9MF6NW2MImJwif1x26pzm4ymHzEfRMZpbxDXBTXNYg6AtjTS4RrHxX07k0YuJVvcz4XeDxWkXavvDyDu4aeMw2cQ27ndqn0+5cuHbHhs6hT2PVHTIvN02x2krssuovSJppWjn/AFD+hTRwChYhjQBsb7qEE+rn2o44GqKmZ7JzkBUmshmPOhlynLDyx7Xj3fZ067LqN0l/fsD/AKkX6F1QdSOkL7ax/wBw/pVRHbs5YBgSaY6mwyv80z1i0oMcXFZbHPzur2Vm8fZePb8ulzH1I6Q/u2X+4f0rlteo3SP9+xH+pF+hVcemTpi4bh37vvKExggPQvRrpv8A44a6dll4+xx7fl077TqN0gK6VYD/AFIv0LkteqmlQ+dpdlsi94XNaRw1YSY0vBFBsEtQq7oiJTE9opQnxvD4y1GWHnj2nHv+zqCPoiKHztKhOoj3pBokIrbE6j8VrQSlezTkxNGDuOGal7JMi9LqDFtp3FdY3aY9PuU4ds+OzqHd0ZpsdhF2rG0tIYhfCTrmCGO1et9RutP75AYYx2baBno0cNO3CBSciLpYrxmxJDXnZ3mty+l6h6UbLTbE3RRfszO6KQG9tyxt245xWOER+OmvVOE3Ocy9rREXndxERAREQEREBERAREQFgrKIPHet0X1zSC/pj8sI7u5VbHXc3GvJVj1ui+uW2Vpl/TCyqyOWq1XLP4qozESZM5pNjMs8pPPYaUMkbRGgmZMSZ4+sJnX2jTtS1jZi892DAMNeq4VlpQ4Y1IncVRHFGXq8w/0TO983et5PrwgamJ3m0qyNwd+bpUiWLaOVZFqajPtDKrYlqwND2iAAzGT0a8OWv4Uu7SDW0jnqBajDVDSoJfvHZUMVrUF3a84+i5Dtrnj9IBbx0JncJ4mrZFr8C8hCoIogJEvm8jJ6u+c5tPzkGkUQo9H30JpwFbm+k+loc9gNJlyBt1X+atY4w1KYyeTzDveKYymStYpUN7SIlNtWTDADzQSgzBEb5OWo9QwrVnOd1SG3gin9I4k0ma3d9L/RUQhynjwfH54mFLC0m1Dupg8nk+yfmlwkDFhJ2Lghnyy8PwqeEmeOwUNQDID5TCgBBybXS9nEqCtGGBeSzNdW3CQ2eCDpgin4bbuZK56vxfWbAtIW1m3+UN6pbLfrpK6bZapK16BP8exF37azw/rBqg97CysBZWVEREBERAREQEREBERAQohQeNdbIvruke0w9UCqqoImachd2dtR318LXrX9tt/vtlQFVNnE1DQ7JXk+NRKaqMWuDT761Exfx9aWojFAa01C7XVb2xNAztK98K7JZ5horSt7zMg5MzI77rsVRDaGTOWDzJlTKgDE7MeworWYZjdLUQMsRlNqRRLfSIpSYFiPO7JDTu1cH9GFRGV0qTF1GAFat/x9KQQghyGmSBUVreZmQ1t6oeK3I1UMjtmN2fpVkt4ornDPdO7DNn1wn+lRRW0iQbyZmTv2qjObzHpZIIIxMkyANKYP92oOP0nDuW12ORm4ek+5tlHKz2mcyFZuzX/hZ9jvNw2tpGC0maU31brm/CgWdpe3Ot8jyAt3OBpRvHceYlCJasZ498uHqhSCKhE5Y0pcdbTyxiQSQxUfGvxOoTPgVLZvKQ52d+q5QQ3XX1aprPnipRE+e85UNbt2SDpgnU6sZm85eM1cdAt+8WPtbP8APDN9ipbMMJY30veoVx0CPrFjd/Fsj/zEkHvqLCysqIiICIiAiIgIiICIiAhRCg8a61T0239oe4Kodg4xzlds1q262fbNI+/hKkO9VIEtryJwM321u3tUYjiYAZXXD5GmZxCjjB7U9TuzyxHOClhJMnmzNkxk92zvMKhtBNsSXy38zVEFrUsW1ljIOTi8nf1Xn2A8VrgQAWwrWTDWZXCLszMQIltIzf3jIi6Xm7OyLoC8EZALU2Mza5irSxN5DBDawmdzyEpk5m+h1sG82Jc9rEXJoaguKVq218Z+ap7Qh4nczZtTPSszdqDNEoYzLJ8Q85sN+r8KCCKU6h8qB5vdV8nwIWsMQxBN30TfIDuG8VKRR6zde+PL7ZMtDS+mJZpPsbvxQZBc1DUzvv2P+H1Z7jm+9vG/H1i2gFZ3+4+D7Mijmkt9SXeW0Sz9aQTWUY95dzxq9duZW8BG9ruXly4KhhG7347+XUtnmZ11vma83ug7AJF8Nebq26B+0WN38Wz/ADwz3qns4nyHvzVv0FF9ZscP21ln6YQe/BZRFlRERAREQEREBERAREQERCg8Y62n67b+08AqqAkX58zlrmytOtw+u2+Ud/3QVUQn6Iuk9x3ZdzKo2cUDuLgaSizlN5a39FQ2z7qT3c+9b2kZc3tm2N3J3BaRBqATwvDFxhe23MKjnijIvOzUMS3xhFBDE8JcXTwzrfcJHFnNeypo7TffLKnZNduQp21AYC8rmaZlK/31vqAg57YsZywmzBgK3SN2IasShtDIm73lncSqGano+auiMXy2hsPNIoJ7O1f2i3PatP6M3wz7LEX4Nd5tAggLngMHfXunhOQhUdoZUYtnNpiW88areKGt+6b0102zHorSKK7xOuWy/bVkGz1rzKnNPvLMEjwmHfLuG31gtYffkJMKeGrErALEPvr3cn/FBMDq2XmVOb81LATflfqvz5vXNCd7Z4/HiuizJMr9+OU/i95QdNhCJnOeBkrnoAfWLH2tl+YXKlhhk9TjKmrmiuurxfSLGf8AOsrq/TCD35ZWAsrKiIiAiIgIiICIiAiIgIiIPGOtsQ/fLev/AJD3Q/FVBNZyE7nYMHy1qz63xNptvd/EO2Q96qzNsK8LiqhG2Gc8LmFRQ/4zoCYLQ1c9wFM+eCltAHwMqO4cGfC/D1Zxx1JfEd+bU5oqIIhh2nbAUZhK96Ne7UiiK57SK+QofpHa78XNfO9EKeMVkBXNxRiL5xN+ID0iRBGbu1ORxahBcSuqcO1SEoIIyMA7gM5kzCeFRqcV7UueOL/INdgQOyO5vw1cqeOCcjN5XNtNGzzesK5yCz4vcfnSUtXnINLU44yBhxLbaNc7NcVz2nHWNZL8eNzKa0BebtOoxl4Uv2FRRGb+HjT5PQIMiK4lr6ZYHmeZW3GWzddzhCogXnzz785bgZceedQcJoIuTfkbm9+pTWADkmZ19/N7hQWdeabOeCmcknwe6c992u9B0wmXGtfgVcdXz9YsPbWf5h7lTwXi/wCb0NZcFbdXT9YsfbWX5wfeg/QQWURZUREQEREBERAREQEREBEQoPFut4+u2+H7THIUCqBGwlUnbs+MsVadcT9d0h5fxPAbVVxAFs+524neqjUNiZaibzLcK4ToX54w8JNCL8Bq37l0RCuLTZzOlc2O6dC8NoZZazgXm9br+Cohih1PNnhJFMNuXnNWOXMYSMTKsqUli88icgV0RRDUJi/VWt+udXihXPam4VlT4MDdKk/6e0ggtImIM+GcyS+uYuL+iorWMXUJ1bNXFp+cpDvdtV9xvltadIVBEZB8zhfUvl+qqCInE0avi2sUxl6S0uyzzMqX8NjrMR994pldIvk8pkrWNqZ93xbKgo6DBkfDfztzC37TDjfwOzh6s44Yvnvbn4LMOpvn8HfL1SgnhhlN6Ufw57nls457qc8c1zwxHCeW27mingGQp3d3OIQdVhTU+D8OZK46A+0WHtrL84uVLZ8cO7VXirrq/wDaLD21n+cOg/QaIiyoiIgIiICIiAiIgIiICFEQeJ9cPt2kTH/k8AqoRZSnR+cZXq164H69b+0NNQ+CqGwIenCh1yleqjNq060FJ437OFJFc8Yf6TY1Z7zS8O+5SEkiYeQzxM8bt+DrERAunibxneZ81VHHFG15faO7Vnff2VFawXM8hUVLyGuowoLypI4nD3gjKcyHOpzkz3QqCOItc3ulQa2b1mvcBFFUlxvcnhtzb1ZwWkU5a/E13vn2slLFGDf4ic2INabZD0SoTFfOTYYu42z40Qc8ZxNGvbbkJnU+ctCLr9kp4bGbZRypIiH5EsXursd7wozgbnlfu8M2QbwkYz5534BIWfDlpFnr3ZRBawFzXvzqb6Psynnbv2Fxw5dBLCPlzP5Los4hV+R3c5LlhGIuzU9lEx97Z3c01IOqA5NycFcdAEHSbBpfxrK/1xVUliGu5v5Ku+r/ANosPbWX5wg/QqIiyoiIgIiICIiAiIgIiICIiDxHrh9t0j2ngOdiqrMSYXXNjPbqvZW3XH7bpD/3ODDGSpy8ufjVVGbW+8kNXi97vW98yoLQyOc5XljP4KS0NTtMrqAMdbSq7XloIzJ3JOR1985veMVRBaFzKTbGqbpCgL5P6ABhtYADlKbM+TZUbNqkKWMsZ1runhLzeGEM4rWNpapNRgTLJi+LZmFBBE3GkyMaNWXLRPzxAudg4vXi9PwhdFpFOmZFMruZhp9pc8Th5Gt7OSSBPZLh5qCO0LTAm73MBPHv2mTKDtXXatjeDVniXU0RwlP5zvGvDABRGHZXddndr2oMkk33PsqZ7P8Aj6q2q/LufjxwJUfz55uyK2JF7HmjbeOaDos4u73jh4LcNMzA5492xc8EeNc37zXWuiz1Nq3c/BB0QRAceclddXj9ZsPbWf54VS2bc01q36v/AGmw9tZXevD7kH6ICyiLKiIiAiIgIiICIiAiIgIiIPEuuB+vW/tD3CW5U0LNSkhRp1YX6lb9cvtuke0//Ikqd66tmsX021VQiJmL2wOqgOfFhWUEcM3Zzj2ml9IkOpohcMmesncSvY8WFQ0JJqzXz1nBUctoZg4MaX3TNPN1S9WJR2h2gAF857peJr2VLHOZJwIYVkG4M3qtdE/LI8McazpSuT3BBHaPg9RUi4Sd5VqO8lc9qNTUphLbg2XZpNTxxXOMi1/hXYYvWlDbX0uaTYBpbBl5tZoIYrznKRM8Q9aaixwUcUQPJzNb9e2rLMcWV70r4F562yWkQlxl3vxfJ0Eg5qJd13dmtcr+R4nfmFrDhw3Xc8St4Wx5pvnxa8IJbM69/j4/FSjli17eHDKfPBr47Xz5xU7zpfdXn3ZIOiDXU4eHvVz1d+02HtrGn3w4wVNZHLnNXPV+Wk2HtrL88MkH6KCysBZWVEREBERAREQEREBERAQohQeHddC2naQP/s8IZKorSUmcijTLZ5DXcr/ygWHY6QtXkIxBGNsIHeCvnhaSkz5Xvm1GbeqjNpLIsJSeWXD5hQRE4X4guwauPCuSljtMriGdpB/6actcoJUvegE2+lPD5KjntTta7YAJcOBrEoYjLi19STN8hX+l/RYyxxmhaVZPc3GjZtfE0FtFK4sxDOcGIO+eT+igjilzQs9bq7Jm8KG0mJX7jJr55cKqS0JhnQEve+I8a6zcueMlqsMjX3Bjrb1kEVoRTn47PArURcTPv5O25ltFFjzLmQ960tJT91b+dtAgwCMOeSOTLeGJvlzzsUcJxbds515rLzrzzzIIJ4Muaz5kpbI88NvOBXLBFyOWvUwPwz5z8Cg67OKXCuKuurhfSbD21k22McvmqKAy28+9fRdR7Lt6do0IH82GM5CB4zwCD9DOsrmhtVLDGsqkRagrZAREQEREBERAREQEREHn3lS6CjtIYdKs3Js4ezaABz2HcRi+Tl2uL3Ly0R4ZzDbJ91F+kiF531v8msFsYrXRSLK1LkwH/wAcRyYPATk4yVHmNpbib0Iucd1JDlgoorZp0DVLU2c7ISVH030ZpeiREW1lHB63ZeA6rQBtkiqb/qEVXB3+CWi2ii+NS8mcX3tNjPGItES9Q5yDkAcjdhCXqYukjRs5RMea7yoz0pjDxpSg2dwpVYtbW1rPKt+DmXOUK5u1zzN8zrqq606UhNYTwxfn5KI9KhvNN2F08cffVLHdax8g+7w2TJUcVpfOuXO6WwKvj6SBuN+HPOZUf/UBgeCWqyiibbzLf4at/wBpz8udwVR+/ZcVj99OHFLFyLYyz5u52ELeG11d/wA+cVSjSosty7+jdFt7ctZQRRPfCGh2xUG9LRZWcewYlhkvRvJd0ZEIjpUTgGEwWfrOxijGUmBk88HNZ1W8n8xHpP0z/QHMP4iZxaqa16joWhEASS1Wlharss41yWNgcF12diVB0QxKWEqKCzKkhCDZERAREQEREBERAREQEREEVrYQxBogCMwvnOkuoHR1uSY9Fsu0fShhEJ3wsV9QiDzHTfIvoEXmRW1nqtTEN0YKptJ8hVmfM0u0H3oID3MvZ0QeDW3kItfR0wbbD3Rrni8hGkf+3Z/7UX6l+gUQfnz/AOCNI/8Abs/9qL9Sks/INbX6ZDssT+pe/og8NsfIND6WlxbLMDvdWeieQ3RIfPtbWPaB3MvX0QefaB5J+j7JmsgSL4gCd5dfR6J1U0azpAr5EHLZdH2cNIQp4bMC4LdEGGWURAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERB//2Q=='
                        price={98.99}
                        rating={5}
                        />
                    <Product 
                        key={5}
                        id = {5}
                        title="New Apple I-Pd Pro (12.9 inch, wifi, 128GB) - Silver (4th Generation)"
                        image='https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/ipad-pro-12-select-wifi-silver-202003_FMT_WHH?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1583551131102'
                        price={598.99}
                        rating={4}
                        />
                </div>
                <div className="home__row">
                    <Product 
                        key={6}
                        id = {6}
                        title="Samsung LC49RG90SSUXEN 49' Curver LED Gaming Monitor"
                        image='https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SL1000_.jpg'
                        price={199.99}
                        rating={3}
                        />
                </div>
            </div>
        </div>
    )
}

export default Home
