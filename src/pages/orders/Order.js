import React from 'react';
import './Order.css';
import moment from 'moment';
import CurrencyFormat from 'react-currency-format';
import CheckOutProduct from '../../components/checkoutproduct/CheckOutProduct';

function Order({order}) {
    return (
        <div className='order'> 
            <h2>Order</h2>
            <p>{moment.unix(order.data.created).format("MMMM Do YYYY,h:mma")}</p>
            <p className='order__id'>
                <small>{order?.id}</small>
            </p>
            {order.data.basket?.map(item=>(
                <CheckOutProduct 
                    id={item.id}
                    image={item.image}
                    title={item.title}
                    price={item.price}
                    rating={item.rating}
                    hiddenButton
            />
            ))}
            <CurrencyFormat
                renderText={(value)=>(
                    <div>
                        <h3
                            className='order__total'>
                            Order Total : <strong>{value}</strong>
                        </h3>
                    </div>
                )}
                decimalScale={2}
                value={order.data.amouunt / 100}
                displayType={"text"}
                thousandSeparator={true}
                prefix={'$'}
            />
            
        </div>
    )
}

export default Order
