import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBVE_SZza9ZwUkfksybpYX1xM6M-yRmFI4",
    authDomain: "clone-7043b.firebaseapp.com",
    databaseURL: "https://clone-7043b.firebaseio.com",
    projectId: "clone-7043b",
    storageBucket: "clone-7043b.appspot.com",
    messagingSenderId: "20920292375",
    appId: "1:20920292375:web:61fb50e88322da9ed13852",
    measurementId: "G-DXT8VL92EK"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
   
const db = firebaseApp.firestore();

const auth = firebase.auth();

export {db, auth};