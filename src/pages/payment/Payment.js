import React,{useState, useEffect} from 'react';
import { Link, useHistory } from 'react-router-dom';
import CheckOutProduct from '../../components/checkoutproduct/CheckOutProduct';
import { useStateValue } from '../../contextAPI files/StateProvider';
import './Payment.css';
import {useStripe,useElements,CardElement} from "@stripe/react-stripe-js";
import CurrencyFormat from 'react-currency-format';
import { basketSum } from '../../contextAPI files/reducer';
import axios from '../../axios';
import { db } from '../../firebase/firebase';

function Payment() {
    const history = useHistory();
    const [{basket,user},dispatch]=useStateValue();
    const [error,setError] = useState(null);
    const [disabled,setDisabled] = useState(true);
    const [succeeded,setSucceeded] = useState(false);
    const [proccessing,setProccessing] = useState(false);
    const [clientSecret,setClientSecret] = useState(true)

    const stripe = useStripe();
    const elements = useElements();

    useEffect(()=>{
        const getClientSecret = async ()=>{
            const responce = await axios({
                method:'post',
                url:`/payments/create?total=${basketSum(basket)*100}`
            });
            setClientSecret(responce.data.ClientSecret);
        }
        getClientSecret();
    },[basket]);

    console.log("THE SECRET IS >>> ", clientSecret);

    const hadleSubmit = async (e) => {
        e.preventDefault();
        setProccessing(true);

        const payload = await stripe.confirmCardPayment(clientSecret,{
            payment_method:{
                card:elements.getElement(CardElement)
            }
        }).then(({paymentIntent})=>{

            db
                .collection('users')
                .doc(user?.uid)
                .collection('orders')
                .doc(paymentIntent.id)
                .set({
                    basket:basket,
                    amouunt: paymentIntent.amount,
                    created:paymentIntent.created
                })

           setSucceeded(true);
           setError(null);
           setProccessing(false);
           dispatch({
               type:"EMPTY_BASKET"
           })
           history.replace('/orders')
        })
    };

    const hadleChange = (e) => {
        setDisabled(e.empty);
        setError(e.error?e.error.message:"")
    };

    return (
        <div className='payment'>
            <div className="payment__container">
                <h1>
                    Checkout (<Link to='/checkout'>{basket?.length} items</Link>)
                </h1>
                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Delivery address</h3>
                    </div>
                    <div className="payment__adress">
                        <p>{user?.email}</p>
                        <p>123 React Lane</p>
                        <p>Los Angeles, CA</p>
                    </div>

                </div>
                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Review items and delivery</h3>
                    </div>
                    <div className="payment__items">
                        {basket?.map((item,i)=>(
                            <CheckOutProduct 
                                key={i}
                                id={item.id}
                                image={item.image}
                                title={item.title}
                                price={item.price}
                                rating={item.rating}
                            />
                        ))}
                    </div>
                </div>
                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Payment Methord</h3>
                    </div>
                    <div className="payment__details">
                        <form onSubmit={(e)=>hadleSubmit(e)}>
                            <CardElement onChange={(e)=>hadleChange(e)}/>
                            <div className="payment__priceContainer">
                                <CurrencyFormat
                                    renderText={(value)=>(
                                        <div>
                                            <p>
                                                Subtotal ({basket.length} items) : <strong>{value}</strong>
                                            </p>
                                        </div>
                                    )}
                                    decimalScale={2}
                                    value={basketSum(basket)}
                                    displayType={"text"}
                                    thousandSeparator={true}
                                    prefix={'$'}
                                />
                                <button disabled={proccessing || disabled || succeeded}>
                                    <span>{proccessing? <p>proccessing</p>:"Buy Now"}</span>
                                </button>
                            </div>
                                    {error && <div>{error}</div>}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Payment
