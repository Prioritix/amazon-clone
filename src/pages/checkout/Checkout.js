import React from 'react';
import CheckOutProduct from '../../components/checkoutproduct/CheckOutProduct';
import { useStateValue } from '../../contextAPI files/StateProvider';
import './Checkout.css'
import Subtotal from './Subtotal';
import FlipMove from 'react-flip-move';

function Checkout() {
    const [{basket},dispatch]=useStateValue();

    
           
    return (
        <div className='checkout'>
            <div className="checkout__left">
                <img
                    className='checkout__ad'
                    src='https://images-eu.ssl-images-amazon.com/images/G/31/img17/pc_banner_2.jpg'
                    alt=''/>
                <div>
                    <h2 className='checkout__title'>
                        Your Shopping Basket
                    </h2>
                    {basket?.map((item,i)=>(
                        <CheckOutProduct 
                            key={i}
                            id={item.id}
                            image={item.image}
                            title={item.title}
                            price={item.price}
                            rating={item.rating}
                            />)
                    )}
                    
                </div>
                
            </div>
            <div className="checkout__right">
                <Subtotal />
            </div>
        </div>
    )
}

export default Checkout
