const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const stripe = require('stripe')("sk_test_51HpX4KCkZaVLC0MoDvQ4ztR6WfmaPvMq5zjcQTmkqaH2UXhH9vtPoPx1pti3O38zivbJziKQCw9uqbuwF9f1HNWE00aFxcVCI1");

//API

//API-Cofig
const app=express();

//Middlewares
app.use(cors({origin:true}));
app.use(express.json());

//API-routes
app.get('/',
    (req,res)=>{
        res.status(200).send('Hello world');
    })

app.post('/payments/create',
    async (req,res)=>{
        const total = req.query.total;
        console.log("Payment request recieved!!",total);
        const paymentIntent = await stripe.paymentIntents.create({
            amount:total,
            currency:"usd"
        }).catch(error=>console.log("Error in line 28 >>>",error));
        res.status(201).send({
            ClientSecret:paymentIntent.client_secret
        }) 
    })

//Listen-Command
exports.api = functions.https.onRequest(app);