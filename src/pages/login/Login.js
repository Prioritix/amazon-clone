import React,{ useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { auth } from '../../firebase/firebase';
import './Login.css'

function Login() {
    const history = useHistory();
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const signIn = (e)=>{
        e.preventDefault();
        auth.signInWithEmailAndPassword(email,password)
        .then((result)=>{
            history.push('/')
        })
        .catch((error)=>alert(error.message))
    };
    const register = (e)=>{
        e.preventDefault();
        auth.createUserWithEmailAndPassword(email,password)
        .then((result)=>{
            console.log(result);
            history.push('/')
        })
        .catch((error)=>alert(error.message))
    };

    return (
        <div className='login'>
            <Link to='/home'>
                <img 
                    className='login__logo'
                    src = 'https://pngimg.com/uploads/amazon/amazon_PNG6.png'
                    alt=''/>
            </Link>

            <div className="login__container">
                <h1>Sign-in</h1>
                <form>

                    <h5>E-mail</h5>
                    <input
                        value={email}
                        onChange={(e)=>setEmail(e.target.value)} 
                        type='text'/>

                    <h5>Password</h5>
                    <input 
                        value={password}
                        onChange={(e)=>setPassword(e.target.value)}
                        type='password'/>

                    <button 
                        type='submit'
                        onClick={signIn}
                        className='login__signInButton'>
                            Sign In</button>

                </form>
                <p>
                    By signing in you agree to Amazon's Conditions of Use & Sale.
                    Please see our Privacy Notice, our Cookie Notice and our Interest
                    based Ads Notice
                </p>

                <button 
                    onClick={register}
                    className='login__registerButton'>
                        Create your Amazon Account</button>
            </div>
            
        </div>
    )
}

export default Login
